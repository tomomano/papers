# papers

* [Sitzmann et al., arXiv (2020) SIREN](ml/sitzmann_2020_siren/README.adoc)
* [Barron et al., arXiv (2020) GHT](ml/barron_2020_GHT/README.adoc)


## Writing asciidoctor

Installation:

```bash
bundle install
```

Generate HTML document:

```
cd <project directory>
bundle exec asciidoctor README.adoc
```
