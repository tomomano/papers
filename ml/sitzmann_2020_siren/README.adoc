= Implicit Neural Representations with Periodic Activation Functions

Sitzmann et al., arXiv (2020) https://arxiv.org/abs/2006.09661

:sectnums:
:source-highlighter: rouge
:rouge-style: github
:icons: font
:xrefstyle: short
:stem: latexmath

== 要旨

* Implicit Neural Representation において， sin関数を活性化関数として使用することを提唱 (SIRENと命名)．
* 従来的な活性化関数 (ReLU, tanh など) に比べて SIREN は高い精度の表現を獲得することに成功．さらにこれらの表現は微分可能である．
* SIREN を使って，画像・音声・ポイントクラウドなどの高精度な再構成を実現．
* SIREN を使って， Helmholtz equation を近似することに成功．

== 背景: Implicit Neural Representation

[stem]
++++

F \left( \mathbf{x}, \Phi, \nabla_{\mathbf{x}} \Phi, \nabla^2_{\mathbf{x}}, ...\right) = 0; \hspace{3mm}
\Phi : \mathbf{x} \mapsto \Phi (\mathbf{x})

++++

のような関係式で定義されるような関数 stem:[\Phi] は一般に陰関数と呼ばれる．

円の方程式 stem:[x^2 + y^2 = 1] や拡散方程式 stem:[\frac{\partial \phi}{\partial t} - D \nabla^2 \phi = 0] などがその例である．

陰関数から解析的に陽関数 (stem:[y = f(x)]) が求まれば簡単であるが，一般的にそれは困難である．
従って，陰関数で定義される関数の評価には，数値計算などが必要になる．

また，画像や音声などの信号も，ある種の陰関数とみなすことができる．
卑近な例でいえば
https://www.wolframalpha.com/input/?i=Hatsune+Miku%E2%80%90like+curve&assumption=%22ClashPrefs%22+-%3E+%7B%22PopularCurve%22%2C+%22HatsuneMikuCurve%22%7D[初音ミクの絵がsinなどの関数を複雑に組み合わせることで表現できるように]
(<<miku>>)，原理上任意の信号を近似する関数を定義することができるはずである．
このような信号の再構成も，ターゲットとなる信号が与えられているという意味で，陰関数と捉えることが可能である．

[[miku]]
.初音ミクを関数で描く
image::hatsune_miku_function.png[hatune_miku, 400]

https://en.wikipedia.org/wiki/Universal_approximation_theorem[万能近似定理]で証明されているように，フィードフォワードニューラルネットワークは任意の関数を近似することができる．
もし，ニューラルネットを使うことで陰関数を近似することができたならば，計算・メモリー使用の効率化など様々な応用が期待できる．
そのように，ニューラルネットを使って陰関数の近似を行うことを **Implicit Neural Representation (INR)** と呼ぶ．
Implicit Neural Representation は，近年のニューラルネットの研究において主要な興味の一つで，上で述べた微分方程式の解の近似や，画像・音声などのデータの再構成に用いられている．

ニューラルネットで陰関数を近似するという問題は，

[stem]
++++
C_m \left(\mathbf{a}(\mathbf{x}), \Phi(\mathbf{x}), \nabla \Phi (\mathbf{x}),... \right) = 0, \forall \mathbf{x} \in \Omega_m \, m = 1, ..., M
++++

というM個の拘束条件を同時に満たすようなニューラルネットを探索する最適化問題である，と定式化することができる．
したがって，最適化すべき損失関数は次式で与えられる．

[stem]
++++
\mathcal{L} = \int_{\Omega} \mathbf{1}_{\Omega_m} (\mathbf{x}) \left\Vert C_m \left(\mathbf{a}(\mathbf{x}), \Phi(\mathbf{x}), \nabla \Phi (\mathbf{x}),... \right) \right\Vert d\mathbf{x}
++++

ここで stem:[\mathbf{1}_{\Omega_m} (\mathbf{x}) = 1] if stem:[\mathbf{x} \in \Omega_m],  stem:[\mathbf{1}_{\Omega_m} (\mathbf{x}) = 0] if stem:[\mathbf{x} \notin \Omega_m] である．

== SIREN

著者らは， INR を解くための有効な手段として，sin 関数を活性化関数として使用したニューラルネットワークである，SIREN (sinusoidal representation networks) を提唱している．
活性化関数以外は，一般的な多層フィードフォーワードネットワークと変わらず，アイディアとしてはいたってシンプルである．

sin 関数を活性化関数として用いるというアイディアは，かなり昔から研究されていた．
SIREN の新しいところは， Implicit Neural Representation の問題に， sin活性化関数を用いると非常に高精度に信号を復元できる点を発見した点である．

SIRENでの各層でのアクティベーションは，次式で与えられる．

[stem]
++++
\mathbf{x}_i \mapsto \phi_i (\mathbf{x_i}) = \sin (\mathbf{W}_i \mathbf{x}_i + \mathbf{b}_i)
++++

stem:[\mathbf{W}_i \mathbf{x}_i + \mathbf{b}_i] の部分はi番目のレイヤーにおけるの Affine 変換である．

SIREN の特徴は，**SIRENのネットワークのn階微分もまた，SIRENであることである**．
この特徴によって，微分された信号を含むような陰関数の近似 (stem:[\nabla \Phi] のターム)がうまくいくと著者らは主張している．

著者らは， SIREN のネットワークを学習させるときの，パラメータの初期化についても議論を行っている．
結論としては， stem:[n] 個の入力を受け取る stem:[i] 層目の重み stem:[\omega_i] は，stem:[\omega_i \sim \mathcal{U}(-\sqrt{6/n}, \sqrt{6/n})] (stem:[\mathcal{U}] は一様分布関数) に従って初期化されるべきであると導いている．
こうすることで，Affine変換後の出力 (sin活性化関数の入力) が stem:[\mathcal{N}(0, 1)] の正規分布に従うことになり，学習がスムーズに進行するとしている．

上記のような処理により，sin活性化関数の入力の大部分は stem:[[-\pi/2, \pi/2]] の範囲に収まるので，この領域については単調増加関数となる．
一方で，入力の一部はとても大きな値を持ちうるので，sin関数の長周期の成分も存在する．
学習に連れて入力の分布がどのように変化していくのか，長周期の成分はどのような寄与をもっているのか，などの点は本論文では詳しく議論されていなかった．
今後の課題であろう．

=== SIREN を使った画像の再構成

SIREN を使った最もシンプルな問題として，著者らはRGB画像の再構成を行った．

画像の再構成は， stem:[\mathbf{x}_i] 番目のピクセルのニューラルネットの出力 stem:[\Phi(\mathbf{x_i})] を実際の画像のRGB値 stem:[f(\mathbf{x})] に近づけるという陰関数近似問題として捉えることができる．
従って，損失関数はシンプルに stem:[\mathcal{L} = \sum_i \left\Vert \Phi(\mathbf{x_i}) - f(\mathbf{x_i}) \right\Vert^2] と書くことができる．

結果が <<image_fitting>> である．
ReLU network with Positional Encoding (ReLU P.E.) と SIREN のみが信号を詳細に復元することができた．
しかしながら，1次，2次の微分をとってみると，ReLU P.E. では信号にたくさんのノイズがのっていることが確認できる．
Tanh を使った場合では，復元した画像 (0次の信号)はぼやけているが，その微分では比較的ノイズが少ないことが確認できる．
著者らは，活性化関数の微分のなめらかさが，復元された信号の微分のなめらかさに作用していると分析している．

[[image_fitting]]
.Sitzmann et al., arXiv (2020), Figure 1より．画像の再構成タスクにおいて，SIRENは従来法よりも細かなディテールを表現することができる．また，画像の一次・二次の微分もきれいな信号が復元できる．
image::fig1.png[fig1, 800]

=== SIREN を使った画像の再構成 (2): 微分値だけを与えた場合

SIREN は微分された信号も正しく復元できる，という結果から，著者らは微分値だけをターゲットとして，ネットワークを最適化することで，元の信号を復元できるか，という問題に取り組んだ．
このような，各点の微分だけが与えられ，もとの関数を復元する問題は，二次の微分の場合は Possion 方程式を解くことに相当する．
物理学の例で言えば，各点で与えられたエネルギーポテンシャルの勾配からエネルギー関数全体を再構築する問題に相当する．

以上を式で表現すると，信号を stem:[f]，ニューラルネットの出力を stem:[\Phi] として，以下のような損失関数を最適化する．

[stem]
++++
\mathcal{L}_{\mathrm{grad}} = \int_{\Omega} \Vert \nabla_x \Phi(\mathbf{x}) - \nabla_x f(\mathbf{x}) \Vert d\mathbf{x}
++++

または，

[stem]
++++
\mathcal{L}_{\mathrm{lapl}} = \int_{\Omega} \Vert \Delta_x \Phi(\mathbf{x}) - \Delta_x f(\mathbf{x}) \Vert d\mathbf{x}
++++

筆者らは，画像の入力データに対して，一次または二次の微分値のみを与えて，上記の損失関数を最小化するようニューラルネットのパラメータを最適化させた．
結果が <<image_fitting_by_grad>> である．
微分情報のみしか与えていないのにもかかわらず，元の画像情報をうまく復元できていることがみてとれる．

[[image_fitting_by_grad]]
.Sitzmann et al., arXiv (2020), Figure 3より．各ピクセルでの微分値 (一次または二次) だけを用いて画像の再構成を行った．
image::fig3.png[fig3, 500]

=== SIREN の応用：そのほかの自然信号

画像以外にも，筆者らは様々な自然界の信号に対して SIREN を用いた信号の復元を試みている．
論文では

* 動画
* 音声
* SDF (signed distance function) -> ポイントクラウドから"表面"を算出するのに用いられる

などで応用が示されている．
詳しくは原著論文，あるいは https://vsitzmann.github.io/siren/[第一著者によるブログ記事] を参照されたい．

== まとめと所感

SIREN は，sin関数を活性化関数に使うという非常にシンプルなアイディアであるが，INRタスクにおいて従来法を大きく上回る性能を発揮した．
シンプルな分，とても応用性が高いアイディアであるということもできる．
ReLU の発見が，2010年ころからのAIの飛躍的性能向上につながったことを鑑みると，今後sin活性化関数についても研究が進むであろう．

また，プラクティカルな応用としても，画像・音声・ポイントクラウドのデータ圧縮などいろいろ応用が考えられる．
また，物理で使われる関数をニューラルネットで近似するという考え方は，最近流行りなので，そちらの方面での応用も期待される．

著者らは，SIRENがうまくいった理由の一つとして，ReLUなどと違ってsin関数はn回微分可能であるkと，SIRENのニューラルネットの微分もまたSIRENである，という点を挙げている．
しかしながら，このあたりの数理的な側面は詳しくは議論されておらず，未解決の問いである．
また，隠れ層での入力・出力の振る舞いについても，著者らは重みパラメータの初期化について考察を行っているが，これが学習を経てどう変化するのか，長周期のsin入力はどんな意味を持つのか，といった点も解明されなければならないだろう．
